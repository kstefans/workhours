# mian.py
import sys
from os import startfile, path
from datetime import datetime as dt
from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font, PatternFill, Alignment


def checkFileExsistance():
    if not path.isfile(fileName):
        wb = Workbook(fileName)
        wb.save(fileName)
        print('WorkHours excel file missing \n\t--> Creating new file')
        print('File name:', fileName)


def createSheetTemplate(wb, sheetName):
    if sheetName not in wb.sheetnames:
        wb.create_sheet(title=sheetName)
        ws = wb[sheetName]
        ws.merge_cells('A1:E2')
        ws['A1'].value = 'Hours of work in ' + str(sheetName)
        ws['A1'].font = Font(size=20, bold=True, color='000000FF')
        ws['A1'].alignment = Alignment(horizontal='center', vertical='center')

        data = ['Day', 'Start time', 'End time', 'Work time', '', 'Work time Sigma']

        for i in range(1, len(data) + 1):
            if data[i - 1] != '':
                char = get_column_letter(i)
                ws[char + str(4)].value = data[i - 1]
                if i % 2 == 0:
                    ws[char + str(4)].fill = PatternFill(fill_type='solid', fgColor='00CCFFFF')
                else:
                    ws[char + str(4)].fill = PatternFill(fill_type='solid', fgColor='0099CCFF')

        ws['F5'] = '=SUM(D5:D35)'
        ws['F5'].number_format = '[h]:mm'

        ws['F7'] = 'Work days Sigma'
        ws['F7'].fill = PatternFill(fill_type='solid', fgColor='00CCFFFF')
        ws['F8'] = '=COUNT(D5:D35)'

        for col in range(2, 5):
            char = get_column_letter(col)
            for row in range(5, 36):
                ws[char + str(row)].number_format = 'hh:mm'



        print('Sheet for', now.strftime('%B'), 'not found \n\t--> Creating new sheet')
        print('Name:', sheetName)

        if 'Arkusz' in wb.sheetnames:
            wb.remove(wb['Arkusz'])
        elif 'Sheet' in wb.sheetnames:
            wb.remove(wb['Sheet'])


def addDate(wb, comm, sheetName):
    ws = wb[sheetName]
    for row in range(5, 36):
        if ws['A' + str(row)].value == dt.now().strftime('%d (%a)') or ws['A' + str(row)].value is None:
            ws['A' + str(row)].value = dt.now().strftime('%d (%a)')
            ws['D' + str(row)].value = '=C' + str(row) + '-' + 'B' + str(row)

            if comm != '' and comm in 'start':
                ws['B' + str(row)].value = dt.now().strftime('%H:%M')
            elif comm == '' or comm in 'end':
                ws['C' + str(row)].value = dt.now().strftime('%H:%M')
            else:
                print('Argument error! Try again.')
            break


if __name__ == "__main__":
    command = None
    now = dt.now()
    sheetName = now.strftime('%m-%Y')
    global fileName
    fileName = 'workHours.xlsx'

    checkFileExsistance()
    wb = load_workbook(fileName)
    createSheetTemplate(wb, sheetName)

    if len(sys.argv) > 1:
        if sys.argv[1] in 'start':
            command = 's'
        elif sys.argv[1] in 'end':
            command = 'e'
        elif sys.argv[1] == '-o':
            startfile(fileName)

    else:
        print('Click enter to set action to \'end\'')
        command = input('Select the \'start\' / \'end\' action to save the data [end]: ')

    addDate(wb, command, sheetName)

    print('\nOperation completed successfully\n\t--> Opening a file')
    print('Added time:', now.strftime('%H:%M'), 'to \'', sheetName, '\'')

    wb.save(fileName)
    startfile(fileName)
